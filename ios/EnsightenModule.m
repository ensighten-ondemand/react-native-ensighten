#import "EnsightenModule.h"

@interface RCT_EXTERN_REMAP_MODULE(Ensighten, EnsightenModule, NSObject)

RCT_EXTERN_METHOD(bootstrap:(NSString *)accountId appId:(NSString *)appId)
RCT_EXTERN_METHOD(trackPageView:(NSString *)page data:(NSDictionary *)data)
RCT_EXTERN_METHOD(trackEvent:(NSString *)event data:(NSDictionary *)data)
RCT_EXTERN_METHOD(trackConversion:(NSString *)page data:(NSDictionary *)data)

@end
