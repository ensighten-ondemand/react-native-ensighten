// EnsightenReactNative.swift

import Foundation
import EnsightenSwift

@objc(EnsightenModule)
class EnsightenModule: NSObject {
  
  @objc(bootstrap:appId:)
  func bootstrap(accountId: String, appId: String) -> Void {
    DispatchQueue.main.async{
      Ensighten.EnsightenBootstrap(accountId: accountId, appId: appId)
    }
  }
  
  @objc(trackPageView:data:)
  func trackPageView(page: String, data: NSDictionary) -> Void {
    Ensighten.trackPageView(page: page, data: data)
  }
  
  @objc(trackEvent:data:)
  func trackEvent(event: String, data: NSDictionary) -> Void {
    Ensighten.trackEvent(event: event, data: data)
  }
  
  @objc(trackConversion:data:)
  func trackConversion(conversion: String, data: NSDictionary) -> Void {
    Ensighten.trackConversion(conversion: conversion, data: data)
  }
  
  @objc
  static func requiresMainQueueSetup() -> Bool {
    return true
  }
  
}
