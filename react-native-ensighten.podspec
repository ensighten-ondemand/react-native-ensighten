require "json"

package = JSON.parse(File.read(File.join(__dir__, "package.json")))

Pod::Spec.new do |s|
  s.name         = "react-native-ensighten"
  s.version      = package["version"]
  s.summary      = package["description"]
  s.description  = <<-DESC
                  react-native-ensighten
                   DESC
  s.homepage     = "https://bitbucket.org/ensighten-ondemand/react-native-ensighten"
  s.license      = "MIT"
  # s.license    = { :type => "MIT", :file => "FILE_LICENSE" }
  s.authors      = { "Brad Slayter" => "brad.slayter@ensighten.com" }
  s.platforms    = { :ios => "9.0" }
  s.source       = { :git => "https://bitbucket.org/ensighten-ondemand/react-native-ensighten.git", :tag => "#{s.version}" }

  s.source_files = "ios/**/*.{h,m,swift}"
  s.requires_arc = true

  s.dependency "React"
  s.dependency "Ensighten"
end

