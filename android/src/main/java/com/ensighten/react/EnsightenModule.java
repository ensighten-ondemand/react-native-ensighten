package com.ensighten.react;

import com.ensighten.Ensighten;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;

public class EnsightenModule extends ReactContextBaseJavaModule {

    public EnsightenModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "Ensighten";
    }

    @ReactMethod
    public void bootstrap(String accountId, String appId) {
        Ensighten.bootstrap(getCurrentActivity(), accountId, appId);
    }

    @ReactMethod
    public void trackPageView(String page, ReadableMap data) {
        Ensighten.trackPageView(page, data.toHashMap());
    }

    @ReactMethod
    public void trackEvent(String event, ReadableMap data) {
        Ensighten.trackEvent(event, data.toHashMap());
    }

    @ReactMethod
    public void trackConversion(String conversion, ReadableMap data) {
        Ensighten.trackConversion(conversion, data.toHashMap());
    }
}
