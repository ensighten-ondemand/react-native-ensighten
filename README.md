# React Native Ensighten Module

## Getting started

`$ npm install react-native-ensighten --save`

### For iOS

```shell
$ cd ios
$ pod install
```

**Special Note:**
Due to a current issue with Xcode where swift support is not enabled when adding a React Native library using swift code
you may need to take the following steps if your build fails (Ignore if you've already added any native swift code to your project):

* Open the `.xcworkspace` file in your project's "ios" directory
* Go to `File > New > File...`
* Select "Swift File" and give it any name
* When prompted select "Add bridging header"
* Rebuild project

### For Android

```shell
$ cd android
$ ./gradlew build
```

## Usage
```javascript
import Ensighten from 'react-native-ensighten';

// In the entry point of your app
Ensighten.bootstap("<account_id>", "<app_id>");
```

**Tracking Page Views**
```javascript
componentDidMount() {
    Ensighten.trackPageView("Product Detail Page", { productId: "12345", productName: "Apple iMac" });
}
```

**Tracking Events**
```javascript
<Button
    title="Add to Cart"
    onPress={() => Ensighten.trackEvent("Add to Cart", { productId: "12345", productName: "Apple iMac" }) };
/>
```

**Tracking Conversions**
```javascript
<Button
    title="Add Promotion to Cart"
    onPress={() => Ensighten.trackEvent("Add Promotion to Cart", { promotionId : "123", promotionName: "Buy an Apple iMac, Get an iPhone 8 50% Off", itemPrice: "$1648.50" }) };
/>
```
